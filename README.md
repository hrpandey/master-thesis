The project involves the development of automatic cloud coverage detection algorithm for infrared and visible wavelength images. Furthermore, a detailed algorithm performance analysis is also presented under `result_evaluation.ipynb`

All the relevant data files are uploaded inside the `csv_files` folder.

`Offset` folder contains the csv files used to develop a mirror temperature offset model. 

`result` folder contains the obtained results from the developed cloud detection algorithm for infrared as well as visible wavelength camera. 
